FROM node:10

WORKDIR /usr/src/app

COPY package*.json ./

RUN apt update && apt install mongodb -y && apt clean
RUN npm install

COPY . .

ENV MONGO_URI=mongodb://localhost:27017/tracing

CMD /bin/bash -c 'mongod --fork --dbpath /data/mongodb/ --logpath /dev/stdout && node server.js'