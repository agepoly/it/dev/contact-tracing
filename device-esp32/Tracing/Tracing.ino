#define READER_ID "M5Stick_1_ded"
#define VERSION "v1.2-09.19"

#define M5STACK // comment to switch to M5Stick
//#define M5STICK // comment to switch to M5Stack

#include <ArduinoJson.h>

#ifdef M5STACK
  #include <M5Stack.h>
#else
  #include <M5StickC.h>
#endif

#include <HTTPClient.h>
#include <Preferences.h>
#include <SimpleCLI.h>
#include <SocketIoClient.h>
#include <WiFi.h>
#include <WiFiClientSecure.h>
#include <Wire.h>

#include "MFRC522_I2C.h"
#include "esp_wpa2.h"

String input;
String inputWifi;

String lastID;

Preferences preferences;

SimpleCLI cli;

Command startWifi;
Command setupWifi;
Command setupTracing;
Command sendManualTracing;

void setupWifiC(cmd* c);
void versionC(cmd* c);
void startWifiFunc();
void startWifiC(cmd* c);
void setupTracingC(cmd* c);
void sendManualTracingC(cmd* c);
void startAPC();

void restartC(cmd* c) {
    Serial.println("Restarting...");
    ESP.restart();
}

void sendPresence(char* id);
void CheckForConnections();

void successScreen(String type, String Pname, bool warning);
void errorScreen(String error);
void connectingScreen();
void loadingScreen();
void waitingScreen();
void ShowReaderDetails();

void arrive(const char * payload, size_t length);
void leave(const char * payload, size_t length);
void error(const char * payload, size_t length);
void socketConnected(const char * payload, size_t length);
void socketDisconnected(const char * payload, size_t length);

String stripAccent(String str);
String abbreviate(String str);

DynamicJsonDocument doc(8096);

HTTPClient http;

int WIFI = 0;

const uint ServerPort = 80;
WiFiServer Server(ServerPort);
WiFiClient RemoteClient;

#define EAP_AN_IDENTITY "anonymous@epfl.ch"
const char* test_root_ca =
    "-----BEGIN CERTIFICATE-----\n"
    "MIIFtzCCA5+gAwIBAgICBQkwDQYJKoZIhvcNAQEFBQAwRTELMAkGA1UEBhMCQk0x\n"
    "GTAXBgNVBAoTEFF1b1ZhZGlzIExpbWl0ZWQxGzAZBgNVBAMTElF1b1ZhZGlzIFJv\n"
    "b3QgQ0EgMjAeFw0wNjExMjQxODI3MDBaFw0zMTExMjQxODIzMzNaMEUxCzAJBgNV\n"
    "BAYTAkJNMRkwFwYDVQQKExBRdW9WYWRpcyBMaW1pdGVkMRswGQYDVQQDExJRdW9W\n"
    "YWRpcyBSb290IENBIDIwggIiMA0GCSqGSIb3DQEBAQUAA4ICDwAwggIKAoICAQCa\n"
    "GMpLlA0ALa8DKYrwD4HIrkwZhR0In6spRIXzL4GtMh6QRr+jhiYaHv5+HBg6XJxg\n"
    "Fyo6dIMzMH1hVBHL7avg5tKifvVrbxi3Cgst/ek+7wrGsxDp3MJGF/hd/aTa/55J\n"
    "WpzmM+Yklvc/ulsrHHo1wtZn/qtmUIttKGAr79dgw8eTvI02kfN/+NsRE8Scd3bB\n"
    "rrcCaoF6qUWD4gXmuVbBlDePSHFjIuwXZQeVikvfj8ZaCuWw419eaxGrDPmF60Tp\n"
    "+ARz8un+XJiM9XOva7R+zdRcAitMOeGylZUtQofX1bOQQ7dsE/He3fbE+Ik/0XX1\n"
    "ksOR1YqI0JDs3G3eicJlcZaLDQP9nL9bFqyS2+r+eXyt66/3FsvbzSUr5R/7mp/i\n"
    "Ucw6UwxI5g69ybR2BlLmEROFcmMDBOAENisgGQLodKcftslWZvB1JdxnwQ5hYIiz\n"
    "PtGo/KPaHbDRsSNU30R2be1B2MGyIrZTHN81Hdyhdyox5C315eXbyOD/5YDXC2Og\n"
    "/zOhD7osFRXql7PSorW+8oyWHhqPHWykYTe5hnMz15eWniN9gqRMgeKh0bpnX5UH\n"
    "oycR7hYQe7xFSkyyBNKr79X9DFHOUGoIMfmR2gyPZFwDwzqLID9ujWc9Otb+fVuI\n"
    "yV77zGHcizN300QyNQliBJIWENieJ0f7OyHj+OsdWwIDAQABo4GwMIGtMA8GA1Ud\n"
    "EwEB/wQFMAMBAf8wCwYDVR0PBAQDAgEGMB0GA1UdDgQWBBQahGK8SEwzJQTU7tD2\n"
    "A8QZRtGUazBuBgNVHSMEZzBlgBQahGK8SEwzJQTU7tD2A8QZRtGUa6FJpEcwRTEL\n"
    "MAkGA1UEBhMCQk0xGTAXBgNVBAoTEFF1b1ZhZGlzIExpbWl0ZWQxGzAZBgNVBAMT\n"
    "ElF1b1ZhZGlzIFJvb3QgQ0EgMoICBQkwDQYJKoZIhvcNAQEFBQADggIBAD4KFk2f\n"
    "BluornFdLwUvZ+YTRYPENvbzwCYMDbVHZF34tHLJRqUDGCdViXh9duqWNIAXINzn\n"
    "g/iN/Ae42l9NLmeyhP3ZRPx3UIHmfLTJDQtyU/h2BwdBR5YM++CCJpNVjP4iH2Bl\n"
    "fF/nJrP3MpCYUNQ3cVX2kiF495V5+vgtJodmVjB3pjd4M1IQWK4/YY7yarHvGH5K\n"
    "WWPKjaJW1acvvFYfzznB4vsKqBUsfU16Y8Zsl0Q80m/DShcK+JDSV6IZUaUtl0Ha\n"
    "B0+pUNqQjZRG4T7wlP0QADj1O+hA4bRuVhogzG9Yje0uRY/W6ZM/57Es3zrWIozc\n"
    "hLsib9D45MY56QSIPMO661V6bYCZJPVsAfv4l7CUW+v90m/xd2gNNWQjrLhVoQPR\n"
    "TUIZ3Ph1WVaj+ahJefivDrkRoHy3au000LYmYjgahwz46P0u05B/B5EqHdZ+XIWD\n"
    "mbA4CD/pXvk1B+TJYm5Xf6dQlfe6yJvmjqIBxdZmv3lh8zwc4bmCXF2gw+nYSL0Z\n"
    "ohEUGW6yhhtoPkg3Goi3XZZenMfvJ2II4pEZXNLxId26F0KCl3GBUzGpn/Z9Yr9y\n"
    "4aOTHcyKJloJONDO1w2AFrR4pTqHTI2KpdVGl/IsELm8VCLAAVBpQ570su9t+Oza\n"
    "8eOx79+Rj1QqCyXBJhnEUhAFZdWCEOrCMc0u\n"
    "-----END CERTIFICATE-----";

SocketIoClient socket;

String selfServiceURL;

MFRC522 mfrc522(0x28);

int lastMessageTime = 0;
int lastRecoTime = 0;
int lastCardTime = 0;

int state = -1;  // -1 = connecting, 0 = waiting, 1 = displaying

// global vars to avoid eeprom read
String baseURL;
String configID;
String type;
String extraArgs;
String Monitor;

void initCmdLine() {
    cli.addCmd("version", versionC);

    startWifi = cli.addCmd("startWifi", startWifiC);

    setupWifi = cli.addCmd("setupWifi", setupWifiC);
    setupWifi.addArg("ssid");
    setupWifi.addArg("user", "");
    setupWifi.addArg("pass");

    setupTracing = cli.addCmd("setupTracing", setupTracingC);
    setupTracing.addArg("baseURL", "tracing.agepoly.ch");
    setupTracing.addArg("configID", READER_ID);
    setupTracing.addArg("type", "auto");
    setupTracing.addArg("extraArgs", "");
    setupTracing.addArg("monitor", "false");  // set to true

    cli.addCmd("restart", restartC);

    sendManualTracing = cli.addCmd("sendManualTracing", sendManualTracingC);
    sendManualTracing.addArg("id");
}

void setup()
{
  M5.begin();
  
  #ifdef M5STACK
    M5.Power.begin();
    M5.Speaker.begin();
  #else 
    M5.Lcd.setRotation(3);
  #endif


  // init tracing args
  preferences.begin("tracing", false);
  baseURL = preferences.getString("baseURL");
  configID = preferences.getString("configID");
  type = preferences.getString("type");
  extraArgs = preferences.getString("extraArgs");
  Monitor = preferences.getString("monitor");
  preferences.end();

  connectingScreen();
  
  Serial.begin(115200);
  Serial.println("Serial Initialized");

  initCmdLine();
  Serial.println("CLI Initialized");
  
  Wire.begin();
  mfrc522.PCD_Init();
  ShowReaderDetails();
  Serial.println("RFID reader Initialized");

    // init tracing args
    preferences.begin("tracing", false);
    baseURL = preferences.getString("baseURL");
    configID = preferences.getString("configID");
    type = preferences.getString("type");
    extraArgs = preferences.getString("extraArgs");
    Monitor = preferences.getString("monitor");
    preferences.end();

    Wire.begin();
    mfrc522.PCD_Init();
    ShowReaderDetails();
    Serial.println("RFID reader Initialized");

    startWifiFunc();
}

void initSocketIO() {
    preferences.begin("tracing", false);

    if (configID == "") {
        Serial.println("Setup tracing using cli and restart wifi");
        return;
    }

    socket.on("connect", socketConnected);
    socket.on("disconnect", socketDisconnected);
    socket.on("arrive", arrive);
    socket.on("leave", leave);
    socket.on("error", error);

    String path = "/socket.io/?configId=" + configID + "&transport=websocket";

    Serial.println("Initializing socket.io at " + baseURL + path);

    socket.beginSSL(baseURL.c_str(), 443, path.c_str());
    preferences.end();
}

void loop() {
    // put your main code here, to run repeatedly:
    CheckForConnections();

    // process socket
    if (isMonitor()) {
        socket.loop();
    }

   // display scan for camipro message
   if(WiFi.status() == WL_CONNECTED && state != 0 && millis() - lastMessageTime > 8000 && millis() > 8000){
       waitingScreen();
   }

    // Display connecting Screen When loosing connection
    if (WiFi.status() != WL_CONNECTED && millis() - lastRecoTime > 10000) {
        Serial.println("Wifi lost, reconnecting.");

        connectingScreen();
        startWifiFunc();
        lastRecoTime = millis();
    }

    // check if remote control
    if (RemoteClient.connected() && RemoteClient.available()) {
        char c = Serial.read();
        inputWifi += c;
        if (c == '\r' or c == '\n') {
            cli.parse(inputWifi);
            inputWifi = "";
        } else {
            RemoteClient.print(c);
        }
    }

    // CHECK IF SERIAL CONTROL
    if (Serial.available()) {
        char c = Serial.read();
        input += c;
        if (c == '\r' or c == '\n') {
            Serial.print(c);
            cli.parse(input);
            input = "";
        } else {
            Serial.print(c);
        }
    }

    // Hanle command errors
    if (cli.errored()) {
        // Get error out of queue
        CommandError cmdError = cli.getError();

        // Print the error
        Serial.print("ERROR: ");
        Serial.println(cmdError.toString());

        // Print correct command structure
        if (cmdError.hasCommand()) {
            Serial.print("Did you mean \"");
            Serial.print(cmdError.getCommand().toString());
            Serial.println("\"?");
        }
        Serial.print("\r\n# ");

        if (RemoteClient.connected()) {
            // Print the error
            RemoteClient.print("ERROR: ");
            RemoteClient.println(cmdError.toString());

            // Print correct command structure
            if (cmdError.hasCommand()) {
                RemoteClient.print("Did you mean \"");
                RemoteClient.print(cmdError.getCommand().toString());
                RemoteClient.println("\"?");
            }
            RemoteClient.print("\r\n# ");
        }
    }

      //Read NFC
    if ( mfrc522.PICC_IsNewCardPresent() && mfrc522.PICC_ReadCardSerial() ) {
      String id = "";
      int count = 0;
      Serial.println("Reading");

        for (byte i = mfrc522.uid.size; i > 0; i--) {
            byte readByte = mfrc522.uid.uidByte[i - 1];

            id = id + (readByte < 16 ? "0" : "") + String(readByte, HEX);
            count++;
        }

        id.toUpperCase();

        if (id != lastID || millis() - lastCardTime > 5000) {
            Serial.print(F("Read card UID:"));
            Serial.println(id);
            sendPresence(const_cast<char*>(id.c_str()));

            Serial.println(lastID);

            lastID = id;
            lastCardTime = millis();
            id = "";
        } else {
            Serial.println("Ignoring already read card " + id +
                           ", retry soon!");
        }
    }

    M5.update();
}

void sendPresence(char* id) {
    String baseUrl = "https://" + baseURL + "/presence/" + configID;
    String url = baseUrl + "/" + type + "?cardid=" + String(id) + extraArgs;
    Serial.println("Requesting : " + url);
    http.begin(url);
    // Send HTTP GET request
    loadingScreen();
    const char* header[] = {"Location"};
    http.collectHeaders(header, 1);

    int httpResponseCode = http.GET();

    if (http.headers() > 0 &&
        (httpResponseCode == 301 || httpResponseCode == 302 ||
         httpResponseCode == 307 || httpResponseCode == 308)) {
        String redirectLocation = http.header("Location");
        if (redirectLocation.startsWith("./")) {
            redirectLocation = baseUrl + "/" + redirectLocation.substring(2);
        } else {
            redirectLocation = baseUrl + redirectLocation;
        }

        http.end();
        Serial.println("Redirected To : ");
        Serial.println(redirectLocation);
        http.begin(redirectLocation);
        http.GET();
    }

    String payload = http.getString();

    if (httpResponseCode > 0) {
        Serial.print("HTTP Response code: ");
        Serial.println(httpResponseCode);
        Serial.println(payload);

        if (payload.indexOf("arrive") != -1) {
            arrive(payload.c_str(), 2048);
        } else if (payload.indexOf("leave") != -1) {
            leave(payload.c_str(), 2048);
        } else {
            Serial.println("Can't find arrive or leave in payload");
            if (payload.length() < 5) {
                errorScreen("Incorrect reply, please retry.");
            } else {
                error(payload.c_str(), 2048);
            }
        }

    } else {
        Serial.print("Error code: ");
        Serial.println(httpResponseCode);
        Serial.println(payload);
        error(payload.c_str(), 2048);
    }
    // Free resources
    http.end();
}

void sendManualTracingC(cmd* c) {
    Command cmd(c);
    char id[16];
    strncpy(id, cmd.getArg("id").getValue().c_str(), 16);
    sendPresence(id);
}

void setupTracingC(cmd* c) {
    Command cmd(c);
    baseURL = cmd.getArg("baseURL").getValue();
    configID = cmd.getArg("configID").getValue();
    type = cmd.getArg("type").getValue();
    extraArgs = cmd.getArg("extraArgs").getValue();
    Monitor = cmd.getArg("monitor").getValue();

    preferences.begin("tracing", false);
    preferences.putString("baseURL", baseURL);
    preferences.putString("configID", configID);
    preferences.putString("type", type);
    preferences.putString("extraArgs", extraArgs);
    preferences.putString("monitor", "false");
    preferences.end();

    Serial.println("OK - Tracing Args Recorded");
}

void setupWifiC(cmd* c) {
    Command cmd(c);

    String ssid = cmd.getArg("ssid").getValue();
    String user = cmd.getArg("user").getValue();
    String pass = cmd.getArg("pass").getValue();

    preferences.begin("wifi", false);
    preferences.putString("ssid", ssid);
    preferences.putString("user", user);
    preferences.putString("pass", pass);
    preferences.end();

    Serial.println("OK - Wifi Credentials Recorded");
}

void versionC(cmd* c) {
    char versionInfo[200];
    snprintf(versionInfo, sizeof(versionInfo), "Version %s, compiled %s at %s \n é ConfigID : ", VERSION, __DATE__, __TIME__, configID);

    Serial.println(versionInfo);
}

void startWifiC(cmd* c) { startWifiFunc(); }

bool isMonitor() { return Monitor == "true"; }

void startWifiFunc() {
    preferences.begin("wifi", false);
    String ssid = preferences.getString("ssid");
    String user = preferences.getString("user");
    String pass = preferences.getString("pass");
    preferences.end();

    Serial.println("Connecting to Wi-Fi: ");
    if (ssid == "") {
        Serial.println("no credentials in memory");
        startAPC();
        return;
    }

    Serial.print("SSID:");
    Serial.println(ssid);
    Serial.print("USER:");
    Serial.println(user);
    Serial.print("PASS:");
    Serial.println("***");

    if (user != "") {
        WiFi.disconnect(
            true);            // disconnect form wifi to set new wifi connection
        WiFi.mode(WIFI_STA);  // init wifi mode
        esp_wifi_sta_wpa2_ent_set_identity((uint8_t*)EAP_AN_IDENTITY,
                                           strlen(EAP_AN_IDENTITY));
        esp_wifi_sta_wpa2_ent_set_username((uint8_t*)user.c_str(),
                                           strlen(user.c_str()));
        esp_wifi_sta_wpa2_ent_set_password((uint8_t*)pass.c_str(),
                                           strlen(pass.c_str()));
        esp_wpa2_config_t config =
            WPA2_CONFIG_INIT_DEFAULT();  // set config settings to default
        esp_wifi_sta_wpa2_ent_enable(
            &config);  // set config settings to enable function

        WiFi.begin(ssid.c_str());  // connect to wifi
    } else {
        WiFi.begin(ssid.c_str(), pass.c_str());
    }

    for (int j = 0; j <= 60; j++) {
        if (WiFi.status() != WL_CONNECTED) {
            delay(500);
            Serial.print(".");
            if (j == 60) {
                Serial.println();
                Serial.println(WiFi.status());
                Serial.printf(
                    "\r\nwrong Wi-Fi credentials. Starting AP.. Going to "
                    "commandline:\r\n");
                Serial.println("ESP32 commandline");
                Serial.print("# ");

                errorScreen("No wifi connection, please restart the device.");

                startAPC();
                break;
            }
        } else {
            Serial.println("done");
            Serial.print("IP address: ");
            Serial.println(WiFi.localIP());
            Server.begin();
            if (isMonitor()) {
                initSocketIO();
            } else {
                waitingScreen();
            }
            break;
        }
    }
}

void startAPC() {
    const char* ssid = "ESP32AP";          // Définir un ssid
    const char* password = "tracing2020";  // Définir un mot de passe
                                           /*
                                           WiFi.disconnect(true);   Serial.println("Creation du Point d'Acces ...");
                                           WiFi.softAP(ssid, password);  // Initialisation avec WiFi.softAP / ssid et
                                           password
                                       
                                           Serial.print("Adresse IP: ");
                                           Serial.println(WiFi.softAPIP());
                                       
                                           M5.Lcd.setCursor(49, 2);
                                           String txt = "AP Mode - ip : "+WiFi.softAPIP();
                                           M5.Lcd.println(txt.c_str()); */
}

void CheckForConnections() {
    if (!Server.hasClient()) return;

    // If we are already connected to another computer,
    // then reject the new connection. Otherwise accept
    // the connection.
    if (RemoteClient.connected()) {
        Serial.println("Connection rejected");
        Server.available().stop();
    } else {
        Serial.println("Connection accepted");
        RemoteClient = Server.available();
        RemoteClient.write("connected");
    }
}

void ShowReaderDetails() {
    // Get the MFRC522 software version
    byte v = mfrc522.PCD_ReadRegister(mfrc522.VersionReg);
    Serial.print(F("MFRC522 Software Version: 0x"));
    Serial.print(v, HEX);
    switch (v) {
        case 0x91:
            Serial.print(F(" = v1.0"));
            break;
        case 0x92:
            Serial.print(F(" = v2.0"));
            break;
        case 0x00:
        case 0xFF:
            Serial.println(
                F("WARNING: Communication failure, is the MFRC522 properly "
                  "connected?"));
            break;
        default:
            Serial.print(F(" (unknown)"));
    }
}

String abbreviate(String str, byte maxLen) {
    if (str.length() > maxLen) {
        return str.substring(0, maxLen - 3) + "...";
    } else {
        return str;
    }
}

void successScreen(String type, String Pname, bool warning, String txt=""){
    if(!warning) {M5.Lcd.fillScreen( GREEN ); M5.Lcd.setTextColor(WHITE, GREEN);} else {M5.Lcd.fillScreen( ORANGE ); M5.Lcd.setTextColor(WHITE, ORANGE);}

    // Message
    #ifdef M5STACK
      M5.Lcd.setTextSize(6);
      M5.Lcd.setCursor(4, 4);
    #else
      M5.Lcd.setTextSize(3);
      M5.Lcd.setCursor(4, 4);
    #endif
    
    M5.Lcd.println(type);

    #ifdef M5STACK
          M5.Lcd.drawLine(0, 60, 300, 60, BLACK);
    #else
          M5.Lcd.drawLine(0, 30, 300, 30, BLACK);
    #endif

    
    // Name
    #ifdef M5STACK
      M5.Lcd.setTextSize(3);
      M5.Lcd.setCursor(2, 65);
      M5.Lcd.print(stripAccent(abbreviate(Pname, 18)));
    #else
      M5.Lcd.setTextSize(1);
      M5.Lcd.setCursor(2, 40);
      M5.Lcd.print(stripAccent(abbreviate(Pname, 26)));
    #endif


    if(warning) {
      // Error
      M5.Lcd.setCursor(2, 50);
      M5.Lcd.println(stripAccent(txt));     
    }
    
    #ifdef M5STACK
      M5.Speaker.tone(550, 200); //beep
    #endif

    state=1;
    lastMessageTime = millis();
}

void errorScreen(String error) {
    M5.Lcd.fillScreen(RED);
    M5.Lcd.setTextColor(WHITE, RED);
    
    #ifdef M5STACK
      M5.Lcd.setTextSize(3);
      M5.Lcd.setCursor(2, 2);
      
      if(error.indexOf("phone.html") != -1){
        M5.Lcd.qrcode("https://go.epfl.ch/ctp");
      }
      
    #else
      M5.Lcd.setTextSize(1);
      M5.Lcd.setCursor(2, 2);
    #endif
   
    M5.Lcd.println(stripAccent(error));

    #ifdef M5STACK
      M5.Speaker.tone(200, 300); //beep
      //delay(3);
      //M5.Speaker.tone(200, 300); //beep
    #endif
    
    state=1;
    lastMessageTime = millis();
}

void connectingScreen() {
    M5.Lcd.fillScreen(BLACK);
    M5.Lcd.setTextColor(WHITE, BLACK);
    
    #ifdef M5STACK
      M5.Lcd.setTextSize(3);
      M5.Lcd.setCursor(32, 10);
    #else
      M5.Lcd.setTextSize(1);
      M5.Lcd.setCursor(32, 10);
    #endif
    
    M5.Lcd.println("Connecting ....");

    char versionInfo[200];
    snprintf(versionInfo, sizeof(versionInfo),
             " Version %s\n Compiled %s\n at %s", VERSION, __DATE__, __TIME__);

    char versionInfo[200];
    snprintf(versionInfo, sizeof(versionInfo), " Version %s\n Compiled %s\n at %s. \n configID : %s", VERSION, __DATE__, __TIME__, abbreviate(configID, 12));
    
    M5.Lcd.setCursor(0, 50);
    M5.Lcd.println(versionInfo);
   
    state=-1;
    lastMessageTime = millis();
}

void loadingScreen() {
    M5.Lcd.fillScreen(WHITE);
    M5.Lcd.setTextColor(BLACK, WHITE);

    #ifdef M5STACK
      M5.Lcd.setTextSize(3);
      M5.Lcd.setCursor(32, 48);
      M5.Lcd.println("Sending,");
      M5.Lcd.setCursor(32, 86);
      M5.Lcd.println("Please wait...");
    #else
      M5.Lcd.setTextSize(2);
      M5.Lcd.setCursor(8, 32);
      M5.Lcd.println("Sending, ");
      M5.Lcd.setCursor(8, 48);
      M5.Lcd.println("Please wait...");
    #endif
    state=0;
    lastMessageTime = millis();
}

void waitingScreen() {
    M5.Lcd.fillScreen(BLACK);
    M5.Lcd.setTextColor(WHITE, BLACK);
    
    #ifdef M5STACK
      M5.Lcd.setTextSize(4);
      M5.Lcd.setCursor(50, 20);
    #else
      M5.Lcd.setTextSize(2);
      M5.Lcd.setCursor(25, 20);
    #endif
        
    if (isMonitor()) {
        M5.Lcd.println("Scan Your \n Camipro or \n  QR below");
    } else {
        M5.Lcd.println("Scan Your \n   Camipro");
    }

    state = 0;
    lastMessageTime = millis();
}

// event functions
void arrive(const char* payload, size_t length) {
    // do stuff
    doc.clear();
    deserializeJson(doc, payload);
    successScreen("Welcome!", doc["person"]["name"],
                  doc["status_code"].as<int>() == 500 ? 1 : 0,
                  doc["status_code"].as<int>() == 500
                      ? doc["message"].as<const char*>()
                      : "");
}

void leave(const char* payload, size_t length) {
    // do stuff
    doc.clear();
    deserializeJson(doc, payload);
    successScreen("See you!", doc["person"]["name"],
                  doc["status_code"].as<int>() == 500 ? 1 : 0,
                  doc["status_code"].as<int>() == 500
                      ? doc["message"].as<const char*>()
                      : "");
}

void error(const char* payload, size_t length) {
    doc.clear();
    deserializeJson(doc, payload);
    errorScreen(doc["message"]);
}

void socketConnected(const char* payload, size_t length) { waitingScreen(); }

void socketDisconnected(const char* payload,
                        size_t length) {  // seems never trigered
    connectingScreen();
}

String getSelfServiceURL() {
    String url = "https://" + baseURL + "/presence/" + configID +
                 "/encodeConfigAndParameters?" + extraArgs;
    Serial.println("Requesting token for QR: " + url);
    http.begin(url);
    // Send HTTP GET request

    int httpResponseCode = http.GET();

    if (httpResponseCode > 0) {
        Serial.print("HTTP Response code: ");
        Serial.println(httpResponseCode);
        String payload = http.getString();
        Serial.println(payload);
        deserializeJson(doc, payload);
        return "https://" + baseURL +
               "/?configId=" + String(doc["token"].as<const char*>()) +
               extraArgs;
    } else {
        Serial.print("Error code: ");
        Serial.println(httpResponseCode);
        return "";
    }

    // Free resources
    http.end();
}

String stripAccent(String str) {
  for(byte i = 0; i < str.length(); ++i) {
    // On regarde le code du caractère correspondant
    Serial.println((unsigned char) str[i]);
    switch((unsigned char) str[i]) {
       case 160:
         // à   
         str[i] = 'a';
         break;
       case 162:
         // â
         str[i] = 'a';
         break;
       case 167:
         // ç
         str[i] = 'c';
         break;
       case 168:
         // è
         str[i] = 'e';
         break;
       case 169:
         // é
         str[i] = 'e';
         break;
       case 170:
         // ê
         str[i] = 'e';
         break;
       case 174:
         // î
         str[i] = 'i';
         break;
       case 185:
         // ù
         str[i] = 'u';
         break;
       case 187:
         // û
         str[i] = 'u';
         break;
     }
  }
  return str;
}
