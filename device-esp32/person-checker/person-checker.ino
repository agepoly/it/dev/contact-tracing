#include <ArduinoJson.h>
#include <M5Stack.h>

#include <Wire.h>

#include "MFRC522_I2C.h"
#include "esp_wpa2.h"

void restartC(cmd* c) {
    Serial.println("Restarting...");
    ESP.restart();
}

void successScreen(String type, String Pname, bool warning);
void errorScreen(String error);
void waitingScreen();
void ShowReaderDetails();

String stripAccent(String str);
String abbreviate(String str);

MFRC522 mfrc522(0x28);

String lastID;
int lastMessageTime = 0;
int lastRecoTime = 0;
int lastCardTime = 0;

int state = -1;  // -1 = not ready, 0 = waiting, 1 = displaying

void setup()
{
    M5.begin();
    M5.Power.begin();
    // M5.Speaker.begin();

    Serial.begin(115200);
    Serial.println("Serial Initialized");

    Wire.begin();
    mfrc522.PCD_Init();
    ShowReaderDetails();
    Serial.println("RFID reader Initialized");
}

void loop() {
    // display scan for camipro message
    if(WiFi.status() == WL_CONNECTED && state != 0 && millis() - lastMessageTime > 8000 && millis() > 8000){
        waitingScreen();
    }

    // Display connecting Screen When loosing connection
    if (WiFi.status() != WL_CONNECTED && millis() - lastRecoTime > 10000) {
        Serial.println("Wifi lost, reconnecting.");

        connectingScreen();
        startWifiFunc();
        lastRecoTime = millis();
    }

    //Read NFC
    if ( mfrc522.PICC_IsNewCardPresent() && mfrc522.PICC_ReadCardSerial() ) {
      String id = "";
      int count = 0;
      Serial.println("Reading");

        for (byte i = mfrc522.uid.size; i > 0; i--) {
            byte readByte = mfrc522.uid.uidByte[i - 1];

            id = id + (readByte < 16 ? "0" : "") + String(readByte, HEX);
            count++;
        }

        id.toUpperCase();

        if (id != lastID || millis() - lastCardTime > 5000) {
            Serial.print(F("Read card UID:"));
            Serial.println(id);
            sendPresence(const_cast<char*>(id.c_str()));

            Serial.println(lastID);

            lastID = id;
            lastCardTime = millis();
            id = "";
        } else {
            Serial.println("Ignoring already read card " + id +
                           ", retry soon!");
        }
    }

    M5.update();
}

void ShowReaderDetails() {
    // Get the MFRC522 software version
    byte v = mfrc522.PCD_ReadRegister(mfrc522.VersionReg);
    Serial.print(F("MFRC522 Software Version: 0x"));
    Serial.print(v, HEX);
    switch (v) {
        case 0x91:
            Serial.print(F(" = v1.0"));
            break;
        case 0x92:
            Serial.print(F(" = v2.0"));
            break;
        case 0x00:
        case 0xFF:
            Serial.println(
                F("WARNING: Communication failure, is the MFRC522 properly "
                  "connected?"));
            break;
        default:
            Serial.print(F(" (unknown)"));
    }
}

String abbreviate(String str, byte maxLen) {
    if (str.length() > maxLen) {
        return str.substring(0, maxLen - 3) + "...";
    } else {
        return str;
    }
}

void successScreen(String type, String Pname, bool warning, String txt=""){
    if(!warning) {M5.Lcd.fillScreen( GREEN ); M5.Lcd.setTextColor(WHITE, GREEN);} else {M5.Lcd.fillScreen( ORANGE ); M5.Lcd.setTextColor(WHITE, ORANGE);}

    // Message
    M5.Lcd.setTextSize(6);
    M5.Lcd.setCursor(4, 4);
    M5.Lcd.println(type);
    M5.Lcd.drawLine(0, 60, 300, 60, BLACK);
    
    // Name
    M5.Lcd.setTextSize(3);
    M5.Lcd.setCursor(2, 65);
    M5.Lcd.print(stripAccent(abbreviate(Pname, 18)));

    if(warning) {
      // Error
      M5.Lcd.setCursor(2, 50);
      M5.Lcd.println(stripAccent(txt));     
    }
    
    // M5.Speaker.tone(550, 200); //beep

    state=1;
    lastMessageTime = millis();
}

void errorScreen(String error) {
    M5.Lcd.fillScreen(RED);
    M5.Lcd.setTextColor(WHITE, RED);
    
    #ifdef M5STACK
      M5.Lcd.setTextSize(3);
      M5.Lcd.setCursor(2, 2);
      
      if(error.indexOf("phone.html") != -1){
        M5.Lcd.qrcode("https://go.epfl.ch/ctp");
      }
      
    #else
      M5.Lcd.setTextSize(1);
      M5.Lcd.setCursor(2, 2);
    #endif
   
    M5.Lcd.println(stripAccent(error));

    #ifdef M5STACK
      // M5.Speaker.tone(200, 300); //beep
      // // delay(3);
      // // M5.Speaker.tone(200, 300); //beep
    #endif
    
    state=1;
    lastMessageTime = millis();
}

void connectingScreen() {
    M5.Lcd.fillScreen(BLACK);
    M5.Lcd.setTextColor(WHITE, BLACK);
    
    #ifdef M5STACK
      M5.Lcd.setTextSize(3);
      M5.Lcd.setCursor(32, 10);
    #else
      M5.Lcd.setTextSize(1);
      M5.Lcd.setCursor(32, 10);
    #endif
    
    M5.Lcd.println("Connecting ....");

    char versionInfo[200];
    snprintf(versionInfo, sizeof(versionInfo),
             " Version %s\n Compiled %s\n at %s", VERSION, __DATE__, __TIME__);

    char versionInfo[200];
    snprintf(versionInfo, sizeof(versionInfo), " Version %s\n Compiled %@@\n at %s. \n configID : %s", VERSION, __DATE__, __TIME__, abbreviate(configID, 12));
    
    M5.Lcd.setCursor(0, 50);
    M5.Lcd.println(versionInfo);
   
    state=-1;
    lastMessageTime = millis();
}

void loadingScreen() {
    M5.Lcd.fillScreen(WHITE);
    M5.Lcd.setTextColor(BLACK, WHITE);

    #ifdef M5STACK
      M5.Lcd.setTextSize(3);
      M5.Lcd.setCursor(32, 48);
      M5.Lcd.println("Sending,");
      M5.Lcd.setCursor(32, 86);
      M5.Lcd.println("Please wait...");
    #else
      M5.Lcd.setTextSize(2);
      M5.Lcd.setCursor(8, 32);
      M5.Lcd.println("Sending, ");
      M5.Lcd.setCursor(8, 48);
      M5.Lcd.println("Please wait...");
    #endif
    state=0;
    lastMessageTime = millis();
}

void waitingScreen() {
    M5.Lcd.fillScreen(BLACK);
    M5.Lcd.setTextColor(WHITE, BLACK);
    M5.Lcd.setTextSize(4);
    M5.Lcd.setCursor(50, 20);
        
    if (isMonitor()) {
        M5.Lcd.println("Scan Your \n Camipro or \n  QR below");
    } else {
        M5.Lcd.println("Scan Your \n   Camipro");
    }

    state = 0;
    lastMessageTime = millis();
}

// event functions
void arrive(const char* payload, size_t length) {
    // do stuff
    doc.clear();
    deserializeJson(doc, payload);
    successScreen("Welcome!", doc["person"]["name"],
                  doc["status_code"].as<int>() == 500 ? 1 : 0,
                  doc["status_code"].as<int>() == 500
                      ? doc["message"].as<const char*>()
                      : "");
}

void leave(const char* payload, size_t length) {
    // do stuff
    doc.clear();
    deserializeJson(doc, payload);
    successScreen("See you!", doc["person"]["name"],
                  doc["status_code"].as<int>() == 500 ? 1 : 0,
                  doc["status_code"].as<int>() == 500
                      ? doc["message"].as<const char*>()
                      : "");
}

void error(const char* payload, size_t length) {
    doc.clear();
    deserializeJson(doc, payload);
    errorScreen(doc["message"]);
}

void socketConnected(const char* payload, size_t length) { waitingScreen(); }

void socketDisconnected(const char* payload,
                        size_t length) {  // seems never trigered
    connectingScreen();
}


String stripAccent(String str) {
  for(byte i = 0; i < str.length(); ++i) {
    // On regarde le code du caractère correspondant
    Serial.println((unsigned char) str[i]);
    switch((unsigned char) str[i]) {
       case 160:
         // à   
         str[i] = 'a';
         break;
       case 162:
         // â
         str[i] = 'a';
         break;
       case 167:
         // ç
         str[i] = 'c';
         break;
       case 168:
         // è
         str[i] = 'e';
         break;
       case 169:
         // é
         str[i] = 'e';
         break;
       case 170:
         // ê
         str[i] = 'e';
         break;
       case 174:
         // î
         str[i] = 'i';
         break;
       case 185:
         // ù
         str[i] = 'u';
         break;
       case 187:
         // û
         str[i] = 'u';
         break;
     }
  }
  return str;
}
