const express = require('express');
const router = express.Router();
const url = require('url');
const crypto = require("crypto");


const jwt = require("jsonwebtoken");
const fetch = require('node-fetch');

const Presence = require('../models/presence');
const Person = require('../models/person');

const apiKey = require('../middleware/apiKey');
const camiproID = require('../middleware/camiproID');
const sciper = require('../middleware/sciper');
const tequila = require('../middleware/tequila');
const unitAccessControl = require('../middleware/unitAccessControl');
const sciperAccessControl = require('../middleware/sciperAccessControl');
const moduloGroupAccessControl = require('../middleware/moduloGroupAccessControl');
const epflGroupAccessControl = require('../middleware/epflGroupAccessControl');
const statusAccessControl = require('../middleware/statusAccessControl');
const externalAccessControl = require('../middleware/externalAccessControl');
const forcePhone = require('../middleware/forcePhone')
const blockExternal = require('../middleware/blockExternal')
const accessControlCheck = require('../middleware/accessControlCheck');
const decodeConfigAndParameters = require('../middleware/decodeConfigAndParameters')

const JWT_PRIV_KEY = process.env.JWT_KEY || crypto.randomBytes(32).toString('hex');
const SMS_API_KEY = process.env.SMS_API_KEY;


const requireNameMail = (req,res,next)=>{
  if(!req.query.email || !req.query.name)
    next("missing get param (cardid|sciper|email&name)");
  else
    next();
}

const requireMail = (req,res,next)=>{
  if(!req.query.email)
    next("missing get param (cardid|sciper|email)");
  else
    next();
}

const countToPath = (cnt)=> ((cnt==0)?'./arrive':'./leave')

router.get('/:configId/auto', camiproID, sciper, requireNameMail, async (req, res, next) => {
  Presence.count({email: req.query.email, $or: [{leftAt: null}, {leftAt: {$gte: Date.now()}}], configId: req.params.configId}).exec()
  .then(count=>res.redirect(countToPath(count)+url.parse(req.url, true).search))
  .catch(err=>next(err));
});

router.get('/:configId/arrive', decodeConfigAndParameters, camiproID, sciper, forcePhone, blockExternal, sciperAccessControl, epflGroupAccessControl, moduloGroupAccessControl, unitAccessControl, statusAccessControl, accessControlCheck, requireNameMail, async (req, res, next) => {
  arrive(req.params.configId, req.query,req.ip)
  .then(rep => {
      res.status(rep.status_code);
      res.json(rep); //to set
  }).catch(err => next(err));
});

const arrive = async (configId, query, ip) => {
  return Presence.count({email: query.email, $or: [{leftAt: null}, {leftAt: {$gte: Date.now()}}], configId: configId}).exec()
  .then( async count=>{
    let rep = {status: "OK - arrive", event: "arrive", status_code: 200, ip: ip};
    if (count != 0) {
      rep.status_code = 500;
      rep.status = "OK Avec Avertissement - arrive";
      rep.message = "Une personne est déja présente avec cet email.\n Elle est maintenant enregistrée plusieurs fois.";
    }

    let presence = new Presence({email: query.email, name: query.name, arrivedAt: Date.now(), configId: configId})

    if(query.sciper) {
      presence.sciper = String(query.sciper);
    }

    if(query.phone) {
      presence.phone = query.phone;
    }

    if(!isNaN(parseInt(query.ACautoQuitAfter, 10)))
    {
      console.log(parseInt(query.ACautoQuitAfter, 10));
      presence.leftAt = new Date(presence.arrivedAt.getTime() + parseInt(query.ACautoQuitAfter, 10));
      console.log(presence);
    }

    await presence.save();

    rep.person = presence;
    rep.person.configId = "hidden";

    global.io.to(configId).to(query.jwtConfigId).emit('arrive', rep);

    return rep;
  })
}

router.get('/:configId/leave', decodeConfigAndParameters, camiproID, sciper, requireMail, async (req, res, next) => {
  leave(req.params.configId, req.query, req.ip)
  .then(rep => {
      res.status(rep.status_code);
      res.json(rep);
  }).catch(err => next(err));
});

const leave = async (configId, query, ip) => {
  return Presence.updateMany({ email: query.email, $or: [{leftAt: null}, {leftAt: {$gte: Date.now()}}], configId: configId}, { leftAt: Date.now() }).exec()
  .then(presence => {
    let rep = {status_code: 200, status: "OK - leave", person: {email: query.email, name: query.name}, nModified: presence.nModified, event: "leave", ip: ip};

    if(query.sciper) rep.person.sciper = String(query.sciper);

    if(presence.nModified < 1){
      rep.status = "Error";
      rep.message = "La personne n'était pas entrée";
      rep.status_code = 500;
    } else if (presence.nModified > 1) {
      rep.status = "OK Avec Avertissement - leave";
      rep.message = "La personne était entrée "+presence.nModified+" fois (sans sortir) \n Toutes les occurrences ont été marquées comme 'Personne Partie'";
      rep.status_code = 500;
    }

    global.io.to(configId).to(query.jwtConfigId).emit('leave', rep)

    return rep;
  });
}

router.get('/:configId/count', decodeConfigAndParameters, async (req, res, next) =>
    Presence.count({ $or: [{leftAt: null}, {leftAt: {$gte: Date.now()}}], configId: req.params.configId}).exec()
    .then(count => res.json(count))
    .catch(err => next(err))
);

router.get('/:configId/list', decodeConfigAndParameters, async (req, res, next) => {

    if (req.query.jwtConfigId && (!req.query.q || req.query.q.length <= 2)) {
       res.status(403);
       return next("Forbidden to list with a jwtConfigId")
    }

    let filter = {$and: [{ $or: [{leftAt: null}, {leftAt: {$gte: Date.now()}}]}, {configId: req.params.configId }]};

    if(req.query.q){
      let search = diacriticSensitiveRegex(req.query.q);
      filter.$and.push({$or: [{email: {$regex: search, $options: 'i'}}, {name: {$regex: search, $options: 'i'}}, {sciper: {$regex: search, $options: 'i'}}]});
      console.log(filter);
    }

    return Presence.find(filter).sort({arrivedAt:-1}).exec()
    .then(presences => res.json(presences))
    .catch(err => next(err));
});

router.get('/:configId/encodeConfigAndParameters', (req, res, next) => {
  try {
    jwt.verify(req.params.configId, JWT_PRIV_KEY);
    return next("already a jwt configID");
  } catch (e) {
    res.json({token: jwt.sign({query: req.query, configId: req.params.configId}, JWT_PRIV_KEY)})
  }
});

router.get('/addMyPhone', tequila, async (req, res, next) => {
    let person = {sciper: req.tequila.sciper, name: req.tequila.name, email: req.tequila.email, units: req.tequila.units, phone: req.query.phone, cardid: req.tequila.cardid};
    Person.findOneAndUpdate({sciper: req.tequila.sciper}, person, {
      upsert: true,
      new: true
    }).exec()
    .then(person => {
      res.set('Content-Type', 'text/html');
      res.send(new Buffer('<h2>Merci Beaucoup ! Vous pouvez maintenant participer à toutes les activités de l\'AGEPoly !</h2>'));
    })
    .catch(err => {
      res.set('Content-Type', 'text/html');
      console.log(err);
      res.send(new Buffer('<h2>Une erreur est survenue : '+err.toString()+'</h2></br><a href="'+req.protocol+'://'+req.host+'/phone.html">Changez/Corrigez le numéro</a>'));
    });
});

router.get('/maybeAddPhone', tequila, (req, res, next) => {
    let person = {sciper: req.tequila.sciper, name: req.tequila.name, email: req.tequila.email, units: req.tequila.units, cardid: req.tequila.cardid};

    key = jwt.sign({sciper: req.tequila.sciper, phone: req.query.phone}, JWT_PRIV_KEY);

    Person.findOneAndUpdate({sciper: req.tequila.sciper}, person, {
      upsert: true,
      new: true
    }).exec()
    .then(person => {
      body = {sms: {message: {text: "Contact Tracing AGEPoly, Merci de validez votre numéro de téléphone : <-short-> ", sender: "AGEPoly", to: req.query.phone, pushtype: "alert", links: ["https://tracing.agepoly.ch/presence/validatePhone?key="+key]}, recipients: {gsm: [{gsmsmsid: req.query.phone, value: req.query.phone}]}}};
      fetch("https://api.smsup.ch/send", {method: 'post', body: JSON.stringify(body), headers: {'Accept': 'application/json', 'Content-Type': 'application/json', 'Authorization': 'Bearer '+SMS_API_KEY}}).then(res => {return res.json()}).then(apires => {
          if(apires.status != 1 && apires.status != -8){
            console.log(apires);
            return Promise.reject("SMS problem, verify your number");
          }
          res.set('Content-Type', 'text/html');
          res.send(new Buffer('<h2>Merci de validez votre numéro avec le lien que vous allez recevoir par SMS ! ('+req.query.phone+')</h2></br><a href="'+req.protocol+'://'+req.host+'/phone.html">Changez/Corrigez le numéro</a>'));
      }).catch(err => {
        res.set('Content-Type', 'text/html');
        console.log(err);
        res.send(new Buffer('<h2>Une erreur est survenue : '+err.toString()+'</h2></br><a href="'+req.protocol+'://'+req.host+'/phone.html">Changez/Corrigez le numéro</a>'));
      });
    })
    .catch(err => {
      res.set('Content-Type', 'text/html');
      console.log(err);
      res.send(new Buffer('<h2>Une erreur est survenue : '+err.toString()+'</h2></br><a href="'+req.protocol+'://'+req.host+'/phone.html">Changez/Corrigez le numéro</a>'));
    });
});

router.get('/validatePhone', (req, res, next) => {


    key = jwt.verify(req.query.key, JWT_PRIV_KEY);

    let person = {phone: key.phone, trusted_phone: true};

    Person.findOneAndUpdate({sciper: key.sciper}, person).exec()
    .then(person => {
      res.set('Content-Type', 'text/html');
      res.send(new Buffer('<h2>Merci Beaucoup ! Vous pouvez maintenant participer à toutes les activités de l\'AGEPoly !</h2>'));
    })
    .catch(err => {
      res.set('Content-Type', 'text/html');
      console.log(err);
      res.send(new Buffer('<h2>Une erreur est survenue : '+err.toString()+'</h2></br><a href="'+req.protocol+'://'+req.host+'/phone.html">Changez/Corrigez le numéro</a>'));
    });

});

const diacriticSensitiveRegex = (string = '') =>
     (string.replace(/a/g, '[a,á,à,ä]')
            .replace(/e/g, '[e,é,ë]')
            .replace(/i/g, '[i,í,ï]')
            .replace(/o/g, '[o,ó,ö,ò]')
            .replace(/u/g, '[u,ü,ú,ù]'))

module.exports = router;
