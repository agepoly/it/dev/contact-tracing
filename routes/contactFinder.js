const express = require('express');
const router = express.Router();

const apiKey = require('../middleware/apiKey');

const Presence = require('../models/presence');
const Person = require('../models/person');


router.get('/deleteOld', async (req, res, next) => {
  const date = new Date();
  date.setTime(date.getTime()-(14 * 24 * 60 * 60 * 1000));
  Presence.remove({
    arrivedAt: {$lte: date},
  }).exec()
  .then(dbres=> {console.log("Removing Old Entries"); console.log(dbres); res.json("OK");})
  .catch(err=> next(err))
});


router.get('/global/list', apiKey, async (req, res, next) =>
  Presence.find({
    $or: [
        {$and: [{arrivedAt: {$gte: req.query.start}}, {arrivedAt: {$lte: req.query.end}}]},
        {$and: [{leftAt: {$gte: req.query.start}}, {leftAt: {$lte: req.query.end}}]},
        {$and: [{arrivedAt: {$lte: req.query.start}}, {leftAt: {$gte: req.query.end}}]},
        ]
  }).exec()
  .then(presences=>res.json(presences))
  .catch(err=> next(err))
);

router.get('/:configId/list', apiKey, async (req, res, next) =>
  Presence.find({
    $and: [
      {$or: [
        {$and: [{arrivedAt: {$gte: req.query.start}}, {arrivedAt: {$lte: req.query.end}}]},
        {$and: [{leftAt: {$gte: req.query.start}}, {leftAt: {$lte: req.query.end}}]},
        {$and: [{arrivedAt: {$lte: req.query.start}}, {leftAt: {$gte: req.query.end}}]},
        ]
      },
      {configId: req.params.configId}
    ]
  }).exec()
  .then(presences=>res.json(presences))
  .catch(err=> next(err))
);

router.get('/global/person', apiKey, async (req, res, next) => {
    search = diacriticSensitiveRegex(req.query.q);
    Presence.find({
      $or: [{email: {$regex: search, $options: 'i'}}, {name: {$regex: search, $options: 'i'}}, {sciper: {$regex: search, $options: 'i'}}]
    }).exec()
    .then(presences=>res.json(presences))
    .catch(err=>next(err));
});


router.get('/:configId/person', apiKey, async (req, res, next) => {
    search = diacriticSensitiveRegex(req.query.q);
    Presence.find({
      configId: req.params.configId,
      $or: [{email: {$regex: search, $options: 'i'}}, {name: {$regex: search, $options: 'i'}}, {sciper: {$regex: search, $options: 'i'}}]
    }).exec()
    .then(presences=>res.json(presences))
    .catch(err=>next(err));
});

router.get('/toNotify', apiKey, async (req, res, next) => {
    Presence.find({email: req.query.email, arrivedAt: {$gte: req.query.start || (Date.now() - 172800000) }}).exec() //default to 2 days
    .then(presences=>{
        var queryFilterTable = [];
        if(presences.length == 0){
          return next("Aucune trace de ce mail dans notre base de donée")
        }
        for (pr in presences){
          queryFilterTable.push({arrivedAt: {$gte: pr.arrivedAt}}, {arrivedAt: {$lte: pr.leftAt}});
          queryFilterTable.push({leftAt: {$gte: pr.arrivedAt}}, {leftAt: {$lte: pr.leftAt}});
          queryFilterTable.push({arrivedAt: {$lte: pr.arrivedAt}}, {leftAt: {$gte: pr.leftAt}});
        }
        Presence.find({email: {$ne: req.query.email}, $or: queryFilterTable}).exec().then(presences => res.json(presences)).catch(err=>next(err));
    })
    .catch(err=>next(err));
});

router.get('/numberList', apiKey, async (req, res, next) =>
  Person.find({}).exec()
  .then(pers=>res.json(pers))
  .catch(err=> next(err))
);

const diacriticSensitiveRegex = (string = '') =>
     (string.replace(/a/g, '[a,á,à,ä]')
            .replace(/e/g, '[e,é,ë]')
            .replace(/i/g, '[i,í,ï]')
            .replace(/o/g, '[o,ó,ö,ò]')
            .replace(/u/g, '[u,ü,ú,ù]'))

module.exports = router;
