//File For Frontend Vue Rendering

// will match one and only one of the string 'true','1', or 'on' rerardless
// of capitalization and regardless off surrounding white-space.
//
const boolRegex = /^\s*(true|1|on)\s*$/i;
const strToBool = (s)=>{
    if(s instanceof Boolean) return s;
    else return boolRegex.test(s);
}

// replace urls by link and add a qr code
const urlRegex = /([ "]?)(https?:\/\/.*?)([ "]{1}|$)/g;
const urlify = (text) => {
    if(!text) return;
    return text.replace(urlRegex, (tot, shit, url, shit2) => {
        let qr = {
            render: 'image',
            minVersion: 1,
            maxVersion: 40,
            ecLevel: 'L',
            left: 0,
            top: 0,
            size: 100,
            fill: '#000',
            background: "white",
            text: url,
            radius: 0.3,
        }
        return '<a href="' + url + '">' + url + '</a></br> ' + $('<div>').qrcode(qr).html() + ' </br>';
    });
}

const code_to_color = (code) => {
    switch(code){
        case '500':
            return 'danger';
        default:
            return 'success';
    }
}

const ip_to_opacity = (self,trg) => {
    return (self == trg) ? 1 : 0.7;
}

const calert = (msg) => {
    //alert(msg);
    console.log(msg);
}


const qr_code_json = (url, size,  radius, center_im) =>{
    return {
    render: 'image',
    minVersion: 1,
    maxVersion: 40,
    ecLevel: 'M',
    left: 0,
    top: 0,
    size: size,
    fill: '#000',
    background: "white",
    text: url,
    radius: radius,
    quiet: 0,
    mode: 4,
    mSize: 0.3,
    mPosX: 0.5,
    mPosY: 0.5,
    image: center_im};
};


Vue.prototype.window = window;

const vm = new Vue({
    el: '#main',
    data: {
        baseUrl: "",
        count: 0,
        max: "",
        arrive_identifier: "",
        arrive_name: "",
        arrive_phone: "",
        leave_select2: null,
        logs: [],
        listePerson: [],
        configId: "",
        hideAlert: false,
        connected: false,
        dispArrive: true,
        timer: null,
        currentLog: 0,
        successSong: null,
        errorSong: null,
        socket: null,
        ip: null,
        modeArrive: true,
        modeLeave: true,
        modeLogs: true,
        modeCounter: true,
        modeList: true,
        modeHome: true,
        modeQR: true,
        modeMobile: false,
        displayBoth: false,
        QRallUrl: "",
        allUrl: "",
        QRtraUrl: "",
        QRtrlUrl: "",
        traUrl: "",
        trlUrl: "",
        pdfDataUris: [],
        ACurlParams: {},
        qrSize: 300,
    },
    methods: {
        updatePresence: function (type,field) {
            return axios.get(this.baseUrl+'/presence/'+this.configId+'/'+type)
            .then(response => this[field] = response.data)
            .catch(error => calert("Erreur : "+error));
        },
        updateCount: function () {
            return this.updatePresence('count','count');
        },
        updateList: function () {
            return this.updatePresence('list','listePerson');
        },

        rLoggin: function (rep, color, type){
            if(this.ip == rep.ip || !this.modeMobile) {
                this.addLog(Object.assign(rep, {color: color, type: type, opacity: ip_to_opacity(this.ip, rep.ip)}))
                this.$forceUpdate();
            }
        },
        addLog: function(log){
            this.currentLog++;
            log.id = this.currentLog;

            log.message = urlify(log.message)

            this.logs.unshift(log);
            this.scroll();

            if(this.modeMobile) setTimeout(() => {this.remLog(log.id)}, 8000)
                return this.currentLog;
        },

        remLog: function(logId){
            this.logs = this.logs.filter((e)=>(e.id !== logId));
            this.scroll();
        },

        rNewStatus: function (rep, type, update_f) {
            update_f(rep);

            this.rLoggin(rep, code_to_color(rep.status_code), type);

            if(this.ip == rep.ip) this.successSong.play();
        },
        rArrive: function(rep){
            this.rNewStatus(rep, 'arrive', (rep)=>{
                this.listePerson.unshift(rep.person);
                this.count++;
                localStorage.setItem('lastPerson', JSON.stringify(rep.person));
            });
        },

        rLeave: function(rep){
            this.rNewStatus(rep, 'leave', (rep)=>{
                this.listePerson = this.listePerson.filter((e) => { return (e.email !== rep.person.email)});
                this.count = this.count - rep.nModified;
            });
        },

        rError: function(rep){
            this.rLoggin(rep, "danger", "error");

            if(this.ip == rep.ip) this.errorSong.play();
        },

        send: function (params, type) {

            switch(type){
                case 'arrive':
                    this.arrive_identifier = "";
                    this.arrive_name = ""
                    this.arrive_phone = ""
                    this.$el.querySelector("#arrive_identifier").focus();
                    break;
                case 'leave':
                    this.leave_select2.val(null).trigger('change');
                    setTimeout(() => this.leave_select2.select2('open'), 100);
                    break;
            }

            reqLog = this.addLog({status: "Envoi ... ", color: "warning", person: params});

            axios.get(this.baseUrl+'/presence/'+this.configId+'/'+type, {params: Object.assign(params, this.ACurlParams)})
            .then((response) => {
                this.remLog(reqLog);
            })
            .catch((error) => {
                this.remLog(reqLog);
                if(error.response == undefined){
                    this.rError({status: "Erreur", message: String(error), color: "danger", type: "Erreur Locale"});
                }
            });
        },

        arrive_change: function () {
            if(this.arrive_identifier.match(/^[0-9]{6}$/))
            {
                this.send({sciper: this.arrive_identifier}, 'arrive');
            }
        },

        update: function(){
            if(this.modeList) this.updateList();
            if(this.modeCounter) this.updateCount();
        },

        scroll: function() {
            let lcontainer = this.$el.querySelector("#logs-container");
            if(lcontainer)
                lcontainer.scrollTop = lcontainer.scrollHeight;
            let pcontainer = this.$el.querySelector("#person-container");
            if(pcontainer)
                pcontainer.scrollTop = pcontainer.scrollHeight;
        },
        genPdf: async function() {
            const pdfUrl = 'AFFICHE_V0.pdf';
            const existingPdfBytes = await axios.get(pdfUrl, {method: 'GET', responseType: 'arraybuffer'}).then(res => res.data);
            const pdfDoc = await PDFLib.PDFDocument.load(existingPdfBytes);
            const helveticaFont = await pdfDoc.embedFont(PDFLib.StandardFonts.Helvetica);
            const pages = pdfDoc.getPages();
            const firstPage = pages[0];
            const { width, height } = firstPage.getSize();

            const pngImageArr = await pdfDoc.embedPng($("#qr-image-tra img")[0].src);

            firstPage.drawText(this.configId, {
                x: 5,
                y: 5,
                size: 10,
                font: helveticaFont,
                color: PDFLib.rgb(0, 0, 0),
            });

            firstPage.drawText('Arrivé', {
                x: 150,
                y: height-100,
                size: 50,
                font: helveticaFont,
                color: PDFLib.rgb(0, 0, 0),
            });

            firstPage.drawRectangle({
                x: 70-15,
                y: height/2-this.qrSize/2-15,
                width: this.qrSize+30,
                height: this.qrSize+30,
                color: PDFLib.rgb(1, 1, 1),
            });

            firstPage.drawImage(pngImageArr, {
                x: 70,
                y: height/2-this.qrSize/2,
                width: this.qrSize,
                height: this.qrSize,
            })

            const pngImageLea = await pdfDoc.embedPng($("#qr-image-trl img")[0].src);

            firstPage.drawText('Départ', {
                x: width-250,
                y: height -100,
                size: 50,
                font: helveticaFont,
                color: PDFLib.rgb(0, 0, 0),
            });

            firstPage.drawRectangle({
                x: width-70-this.qrSize-15,
                y: height/2-this.qrSize/2-15,
                width: this.qrSize+30,
                height: this.qrSize+30,
                color: PDFLib.rgb(1, 1, 1),
            });

            firstPage.drawImage(pngImageLea, {
                x: width-70-this.qrSize,
                y: height/2-this.qrSize/2,
                width: this.qrSize,
                height: this.qrSize,
            });

            return pdfDoc.saveAsBase64({ dataUri: true });
        },
        genPdf2: async function() {
            const pdfUrl = 'AFFICHE_V1.pdf';
            const existingPdfBytes = await axios.get(pdfUrl, {method: 'GET', responseType: 'arraybuffer'}).then(res => res.data);
            const pdfDoc = await PDFLib.PDFDocument.load(existingPdfBytes);
            const helveticaFont = await pdfDoc.embedFont(PDFLib.StandardFonts.Helvetica);
            const pages = pdfDoc.getPages();
            const firstPage = pages[0];
            const { width, height } = firstPage.getSize();

            const pngImageArr = await pdfDoc.embedPng($("#qr-image-tra img")[0].src);

            firstPage.drawText(this.configId, {
                x: 30,
                y: 30,
                size: 10,
                font: helveticaFont,
                color: PDFLib.rgb(0, 0, 0),
            });


            firstPage.drawImage(pngImageArr, {
                x: 45,
                y: height-405,
                width: this.qrSize*0.7,
                height: this.qrSize*0.7,
            })

            const pngImageLea = await pdfDoc.embedPng($("#qr-image-trl img")[0].src);

            firstPage.drawImage(pngImageLea, {
                x: width-45-this.qrSize*0.7,
                y: height-405,
                width: this.qrSize*0.7,
                height: this.qrSize*0.7,
            });

            return pdfDoc.saveAsBase64({ dataUri: true });
        },
        reconnect_socket: function(){
            if(!this.connected){
                setTimeout(() => {
                    this.socket.connect();
                    this.reconnect_socket();
                }, 10000);
            }
        },
    },
    mounted: function(event) {
        this.leave_select2 = $("#leave_identifier").select2({
            placeholder: 'Search for someone',
            minimumInputLength: 3,
            ajax: {
                data: (params)=>{return {q: params.term};},
                dataType: 'json',
                delay: 250,
                url: this.baseUrl+'/presence/'+this.configId+'/list',
                processResults: (data)=>{return {
                    results: $.map(data, (obj) => {
                        return { 
                            id: obj.email, 
                            text: (obj.sciper)?
                                (obj.sciper + " - " + obj.name + " - " + obj.email):
                                (obj.name + " - " + obj.email)
                        };
                    })
                }}
            },
            width: '100%',
        });

        this.leave_select2.on( "change", () => {
            if(this.leave_select2.select2('data')[0])
                this.send({email: this.leave_select2.select2('data')[0].id}, 'leave');
        });

        if(!this.modeHome && this.modeQR){

            axios.get(this.baseUrl+'/presence/'+this.configId+'/encodeConfigAndParameters?'+ (() => {a=''; for (p in this.ACurlParams) {a=a+'&'+p+'='+this.ACurlParams[p]}; return a;})())
            .then(response => {
                this.traUrl = this.baseUrl+'?mob=1&qr=0&noGitLink=1&list=0&leave=0&configId='+response.data.token;
                this.trlUrl = this.baseUrl+'?mob=1&qr=0&noGitLink=1&list=0&arrive=0&configId='+response.data.token;
                this.allUrl =  this.baseUrl+window.location.search;

                $("#qr-image-tra").qrcode(qr_code_json(this.traUrl,this.qrSize, 0.3, $("#center-img-tr")[0]));
                $("#qr-image-trl").qrcode(qr_code_json(this.trlUrl,this.qrSize, 0.3, $("#center-img-tr")[0]));
                $("#qr-image-all").qrcode(qr_code_json(this.allUrl,this.qrSize, 0.25, $("#center-img-all")[0]));

                this.QRtraUrl=$('#qr-image-tra img')[0].src;
                this.QRtrlUrl=$('#qr-image-trl img')[0].src;
                this.QRallUrl=$('#qr-image-all img')[0].src;

                this.genPdf().then(uri => this.pdfDataUris.push(uri)).catch(error => calert("Erreur : "+error));
                this.genPdf2().then(uri => this.pdfDataUris.push(uri)).catch(error => calert("Erreur : "+error));
            })
            .catch(error => calert("Erreur : "+error));
        }
    },
    beforeMount: function(event) {
        const urlParams = new URLSearchParams(window.location.search);

        this.configId = urlParams.get('configId');
        if(this.configId)
            this.modeHome = false;

        this.max = parseInt(urlParams.get('max')) || Infinity; ;
        this.modeArrive = strToBool(urlParams.get('arrive') || this.modeArrive);
        this.modeLeave = strToBool(urlParams.get('leave') || this.modeLeave);
        this.modeLogs = strToBool(urlParams.get('logs') || this.modeLogs);
        this.modeCounter = strToBool(urlParams.get('counter') || this.modeCounter);
        this.modeList = strToBool(urlParams.get('list') || this.modeList);
        this.modeQR = strToBool(urlParams.get('qr') || this.modeQR);

        this.hideAlert = strToBool(urlParams.get('noGitLink') || this.hideAlert);

        this.displayBoth = strToBool(urlParams.get('displayBoth') || (this.modeArrive && this.modeLeave));

        this.modeMobile = strToBool(urlParams.get('mob') || this.modeMobile);

        this.baseUrl = location.protocol+'//'+location.hostname+(location.port ? ':'+location.port: '');

        for ([paramN, paramV] of urlParams.entries()) {
            if(paramN.match(/ACblock.*/) || paramN.match(/ACforce.*/))
                this.ACurlParams[paramN] = strToBool(paramV);
            else if (paramN.match(/AC.*/))
                this.ACurlParams[paramN] = paramV;
        }

        this.update();
        this.timer = setInterval(this.update, 500000)

        this.successSong = new Audio(this.baseUrl+'/success.mp3');
        this.errorSong = new Audio(this.baseUrl+'/error.mp3');

        this.socket = io({
            query: {
                configId: this.configId
            }
        });

        //populate form
        let p = JSON.parse(localStorage.getItem('lastPerson'));
        if (p && p.sciper == undefined){
            this.arrive_phone = p.phone;
            this.arrive_identifier = p.email;
            this.arrive_name = p.name;
        }

        axios.get(this.baseUrl+'/myip')
        .then(response => this.ip = response.data.ip)
        .catch(error => calert("Erreur getting local IP: "+error));

        this.socket.on('arrive', (msg) => this.rArrive(msg));
        this.socket.on('leave', (msg) => this.rLeave(msg));
        this.socket.on('error', (msg) => this.rError(msg));

        this.socket.on('connect', () => this.connected = true);
        this.socket.on('disconnect', () => {
            this.connected = false;
            this.reconnect_socket();
        });
    },
});
