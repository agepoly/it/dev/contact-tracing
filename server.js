// modules =================================================
const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const morgan = require('morgan');

const http = require('http');

const fs = require('fs');
const path = require('path');

let dotenv = require('dotenv')

const tequila = require('./middleware/tequila');

const envLoadResult = dotenv.config()
 
if (envLoadResult.error) {
  console.log("Using env var for configuration")
}
else {
  console.log("Using .env for configuration")
}

const env = process.env.NODE_ENV;


if(env != 'production' && !envLoadResult.error){ 
    console.log(envLoadResult.parsed)
}

const app = express();

if(process.env.USE_PROXY){
    app.set('trust proxy', true);
}

console.log('Application started');


const httpServer = http.createServer(app);

global.io = require('socket.io')(httpServer);


global.io.on('connection', (socket) => {
  socket.join(socket.handshake.query.configId);
});

// configuration

// mongoose
mongoose.connect(process.env.MONGO_URI || 'mongodb://localhost/tracing',{ useUnifiedTopology: true, useNewUrlParser: true }); 

if(env == 'production'){ 
    app.use(morgan('combined'));
}
else {
    app.use(morgan('combined'));
}

app.use((req, res, next) => {
    req.ip = (req.headers['x-forwarded-for'] || '').split(',').pop().trim() || 
         req.connection.remoteAddress || 
         req.socket.remoteAddress || 
         req.connection.socket.remoteAddress;
    next();
});

// set url
app.use(function(req, res, next) {
    req.getUrl = req.protocol + "://" + req.get('host') + req.originalUrl;
    return next();
});

app.use(require('cookie-parser')());

app.get('/myip', (req, res, next) => res.json({ip: req.ip}));

app.use('/phone.html', tequila, express.static(__dirname + '/frontend/phone.html'));

app.use('/', express.static(__dirname + '/frontend/'));


app.use('/presence', require('./routes/presenceManager'));
app.use('/contacts', require('./routes/contactFinder'));

// catch 404 and forward to error handler
app.use((req, res, next) => {res.status(404); res.json({status_code: 404, status: "Error - Not Found", message: "Url not found : "+req.originalUrl});});

app.use(function(err, req, res, next) {
    res.status(err.status_code || 500);
    
    console.error(err);
    console.log(req.connection.remoteAddress);
    
    if(typeof err == String) {
        err = {status_code: 500, message: err, status: "Error"}
    }
    if(!err.message){
        err = {status_code: 500, message: String(err), status: "Error", error_obj: err}
    }
    
    err.ip = req.ip;

    if(req.params.configId != undefined)
    {
        if(req.query.oldConfigId) {
          global.io.to(req.params.configId).to(req.query.jwtConfigId).emit('error', err);
        } else {
          global.io.to(req.params.configId).emit('error', err);
        }
    } else {
        global.io.emit('error', err);
    }
    res.json(err);
});

httpServer.listen(process.env.PORT || 3000, () => console.info("http server started listening"));


module.exports = app;

