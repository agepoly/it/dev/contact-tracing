#!/bin/sh

sudo apt update 
sudo apt install -y chromium xorg unclutter pcscd pcsc-tools libpcsc-perl florence

sudo cp ../device-linux/camipro_to_tracing /usr/bin/

cp ./xinitrc-default /home/pi/.xinitrc
echo $1 > /etc/tracing_url
echo $2 > /etc/tracing_args

sudo cp ./tracing-reader.service /lib/systemd/system/
sudo cp ./tracing-xserver.service /lib/systemd/system/
sudo systemctl enable tracing-xserver
sudo systemctl enable tracing-reader
