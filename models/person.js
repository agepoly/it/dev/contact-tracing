const mongoose = require('mongoose');

const PersonSchema = new mongoose.Schema({
    sciper: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
    },
    units: {
        type: String,
        required: false,
    },
    phone: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    cardid: {
        type: String,
        required: true
    },
    trusted_phone: {
        type: Boolean,
        required: false
    }
});

module.exports = mongoose.model('Person', PersonSchema);
