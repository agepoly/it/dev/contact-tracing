const mongoose = require('mongoose');

const PresenceSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
    },
    sciper: {
        type: String,
        required: false,
    },
    arrivedAt: {
        type: Date,
        required: true
    },
    leftAt: {
        type: Date,
        required: false
    },
    configId: {
        type: String,
        required: true
    },
    phone: {
        type: String,
        required: false,
    },
});

module.exports = mongoose.model('Presence', PresenceSchema);
