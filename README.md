# Contact tracing for EPFL 👉

Using an __NFC Card__ ([ACR122](http://downloads.acs.com.hk/drivers/en/API-ACR122U-2.02.pdf)) reader we obtain the __SCIPER__ and keep track of who is in a room at any given time with __MongoDB__. This can run on __Linux, RPI, ESP32__. There is also the option of just running the __Web Client__

# Linux Client 🐧 

**camipro_to_tracing** is a perl script used to use camipro reader as a client.

For this you need to install the tools, remove kernel modules and start the pcscd server which will listen for cards passed on the reader :ear:

```Bash
sudo apt install perl pcsc-tools pcscd

sudo modprobe -r pn533_usb
sudo modprobe -r pn533

sudo /etc/init.d/pcscd start
```

**Depending on your reader you may have to install other drivers or modprobe things.**

You can then run

```
perl device-linux/camipro_to_tracing http://exemple.com/ arrive kx2ldow6 &ACallowedScipers=123456
```

# Raspberry PI 🥧 

Simply run 

```
install.sh host_url extra_args
```

and your RPI will automatically open the session everytime you start it. 

# Web client (No nfc) 🌍 

At /frontend/index.html on disk, (/ url)  you can find a web client accessing it without query parameters display help

Can serve as a frontend for other clients (All logs and events with a certain configId are sent to all clients configured with this configId)

# IOT Client (ESP32) 📶 

You can also run the client on an ESP32 (Preferably M5Stack). See folder device-esp32

You can borrow these at AGEPoly (truffe2.agepoly.ch)

# Running the Server using NodeJS ⬢

To run the server you must first install npm, which will install node, on which the server runs.

```
sudo apt install npm
```
```
node server.js production
```

# Running the Server using Docker 🐋 

Docker runs the application in a container.

```
sudo apt install docker
```

Run docker, specifying environment variables, a mongodb by mounting a volume and the image, which is fetched from the AGEPoly git.

```
docker run -p 3000:80 -e API_KEYS=yoursecretapikey -v /some/place/on/your/server:/data/mongodb/ registry.gitlab.com/agepoly/it/dev/contact-tracing:latest
```

You can use any configId (and so have multiple room where you track presence) but keep it secret cause you can have the list of all the people in the room at the moment.



# REST Requests ↕ 

**Disclaimer: Check index.html form for the updated url parameters**

### When someone arrives
```
/presence/configId/arrive?email=email&name=name
/presence/configId/arrive?sciper=sciper
/presence/configId/arrive?cardid=cardid
```
### When someone leaves
```
/presence/configId/leave?email=email
/presence/configId/leave?sciper=sciper
/presence/configId/leave?cardid=cardid
```
### Auto leave or arrive based on status
```
/presence/configId/auto?see params before  
```
### List and count people currently in the room
```
/presence/configId/list
/presence/configId/count
```
### Retrieve contacts when asked by canton
```
/contacts/configId/list?start=1970-01-01&end=2100-01-01&apiKey=api_key
/contacts/configId/person?q=search_string&apiKey=api_key
```
Note that some middlewares are translating cardId and sciper to email and name for presenceManager routes.

You should set a mongodb uri as MONGO_URI or in .env

You should provide a coma separated list of api key in API_KEYS, these are master api key used to retrieve contact list.

---

This script take advantage of led and buzzer of ACR122 nfc reader

[Reader data sheet](http://downloads.acs.com.hk/drivers/en/API-ACR122U-2.02.pdf)




